/*
Result:

Satisfaction status: true
*/

import java.lang.ref

import sessl._
import sessl.mlrules._
import sessl.verification._
import sessl.verification.mitl._


// Reference values for calculating the sensitivity analysis results
// Reference = FractionSurfaceLRP6
object Reference {
  val ref: Trajectory[Double] = List(
    (0, 1),
    (10, 0.6408),
    (20, 0.5563),
    (30, 0.4554),
    (60, 0.3944),
    (61, 0.3944)  // Dummy value for MITL to work (Needs one more observation)
  )
}

val factor = 4000 // nR_0 (factor for comparing with measurement data)
val epsilon = 0.2 // Epsilon for allowed relative distance of simulated to measured data

// Statistical Model Checking
execute(

  new Experiment with Observation with ExpressionObservation with StatisticalModelChecking with ParallelExecution with CSVOutput {

    model = "../models/M3_Wnt.mlrj"

    simulator = SimpleSimulator()
    parallelThreads = 1

	//val stoppingTime = Reference.ref.times.last
    stopTime = 61
    //replications = 5

    // Set initial species count as well as experiment specific reaction rate
    // coefficients

    set(
      "nR" <~ factor,
      "nL" <~ 2000,
      "ke_raft" <~ 0.1,
      "ke_nonraft" <~ 0.1
    )

    //observeAt(Reference.ref.times: _*)
    observeAt(0, 10, 20, 30, 60, 61)

    /*
    val Lrp6uPuB = observe("Lrp6uPuB" ~ count("Cell/Membrane/Lrp6(uP, uB)"))
    val Lrp6PuB = observe("Lrp6PuB" ~ count("Cell/Membrane/Lrp6(P, uB)"))
    val Lrp6uPB = observe("Lrp6uPB" ~ count("Cell/Membrane/Lrp6(uP, B)"))
    val Lrp6PB = observe("Lrp6PB" ~ count("Cell/Membrane/Lrp6(P, B)"))
    val Lrp6Axinu = observe("Lrp6Axinu" ~ count("Cell/Membrane/Lrp6Axin(u)"))
    val Lrp6Axinp = observe("Lrp6Axinp" ~ count("Cell/Membrane/Lrp6Axin(p)"))

    val Raft_Lrp6uPuB = observe("Raft_Lrp6uPuB" ~ count("Cell/Membrane/LR/Lrp6(uP, uB)"))
    val Raft_Lrp6PuB = observe("Raft_Lrp6PuB" ~ count("Cell/Membrane/LR/Lrp6(P, uB)"))
    val Raft_Lrp6uPB = observe("Raft_Lrp6uPB" ~ count("Cell/Membrane/LR/Lrp6(uP, B)"))
    val Raft_Lrp6PB = observe("Raft_Lrp6PB" ~ count("Cell/Membrane/LR/Lrp6(P, B)"))
    val Raft_Lrp6Axinu = observe("Raft_Lrp6Axinu" ~ count("Cell/Membrane/LR/Lrp6Axin(u)"))
    val Raft_Lrp6Axinp = observe("Raft_Lrp6Axinp" ~ count("Cell/Membrane/LR/Lrp6Axin(p)"))
    */

    val Lrp6 = observe("Lrp6" ~ count("Cell/Membrane/Lrp6(_, _)"))
    val Lrp6Axin = observe("Lrp6Axin" ~ count("Cell/Membrane/Lrp6Axin(_)"))
    val Raft_Lrp6 = observe("Raft_Lrp6" ~ count("Cell/Membrane/LR/Lrp6(_, _)"))
    val Raft_Lrp6Axin = observe("Raft_Lrp6Axin" ~ count("Cell/Membrane/LR/Lrp6Axin(_)"))

    //val allR = observe(Expr(N => N(Lrp6uPuB) + N(Lrp6PuB) + N(Lrp6uPB) + N(Lrp6PB) + N(Lrp6Axinu) + N(Lrp6Axinp) +
    //  N(Raft_Lrp6uPuB) + N(Raft_Lrp6PuB) + N(Raft_Lrp6uPB) + N(Raft_Lrp6PB) + N(Raft_Lrp6Axinu) + N(Raft_Lrp6Axinp)))
    val allR = observe(Expr(N => N(Lrp6) + N(Lrp6Axin) + N(Raft_Lrp6) + N(Raft_Lrp6Axin)))

    test = SequentialProbabilityRatioTest(p = 0.8, alpha = 0.05, beta = 0.05, delta = 0.05)
    //G: globally, F: Eventually
    prop = MITL( G(Reference.ref.times.head, Reference.ref.times.last - 1)( (OutVar(allR)/Constant(factor) - Traj(Reference.ref)).abs < Constant(epsilon)) )

    // Alternative without MITL (works better):
    //prop = (_, outputs) => {
    //  val values = outputs(allR).asInstanceOf[Trajectory[Double]].toMap
    //  Reference.ref.forall { case (t, y) => (values(t)/factor - y).abs/(y).abs < epsilon }
    //}

    withCheckResult { result =>
      println(s"Satisfaction status: ${result.satisfied}")
    }

    withExperimentResult(writeCSV)
  }
)