/*
Results:

parameter ke:
  - individual effect:         -0.07323514015765209
  - total effect:              -0.07323514015765209
  - total interaction effect:  0.0
*/

import sessl._
import sessl.mlrules._
import sessl.analysis.sensitivity._

// Reference values for calculating the sensitivity analysis results
// Reference = FractionSurfaceLRP6
object Reference {
  val ref: Trajectory[Double] = List(
    (0, 1),
    (10, 0.6408),
    (20, 0.5563),
    (30, 0.4554),
    (60, 0.3944)
  )
}

val factor = 4000 // nR_0 (factor for comparing with measurement data)


// Sensitivity analysis
analyze((params, objective) =>
  execute(

    new Experiment with Observation with ExpressionObservation with ParallelExecution with CSVOutput {

      model = "../models/M1_General.mlrj"

      simulator = HybridSimulator()
      //simulator = StandardSimulator()
      parallelThreads = 1

      val stoppingTime = Reference.ref.times.last + 1
      stopTime = stoppingTime
      replications = 1

      // Set initial species count as well as experiment specific reaction rate
      // coefficients

      set(
        "nR" <~ factor,
        "nL" <~ 2000
      )

      //observeAt(Reference.ref.times: _*)
      observeAt(Reference.ref.times.last)
      //observeAt(0, 10, 20, 30, 60)
      val R = observe("R" ~ count("M/R")) // Surface receptors
      val LR = observe("LR" ~ count("M/LR")) // Surface bound receptors
      val allR = observe(Expr(N => N(R) + N(LR)))

      for ((name, value) <- params) {
        set(name <~ value)
      }

      /*
      withExperimentResult(results => {
        val distances = for (run <- results.runs) yield {
          val trajectory = run.trajectory(allR)
          val trajectory2 = trajectory.map {
            case (t, y) => (t, y / factor)
          }
          Misc.rmse(trajectory2, Reference.ref)
        }
        objective <~ distances.sum / distances.size
      })
      */
      withExperimentResult(results =>
        objective <~ results.mean(allR)
      )

      withRunResult(writeCSV)

    }
  )
) using new TwoLevelFullFactorialSetup {

  baseCase(
    "ke" <~ 0.2
  )
  sensitivityCase(
    "ke" <~ 0.4
  )

  withAnalysisResult(result => {
    result.printCompleteReport()
  })
}
