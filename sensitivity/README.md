# How to execute the sensitivity analysis experiments

* install [Apache Maven](https://maven.apache.org/)
* adapt the name of the script you would like to use in the pom.xml file
* run ```./mvnw scala:script```
