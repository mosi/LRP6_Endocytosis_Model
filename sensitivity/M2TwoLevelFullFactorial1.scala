/*
Results:

parameter ke_raft:
  - individual effect:         0.04672422607197643
  - total effect:              0.028517366666842237
  - total interaction effect:  -0.018206859405134196

parameter ke_nonraft:
  - individual effect:         0.10001470160401424
  - total effect:              0.08180784219888004
  - total interaction effect:  -0.0182068594051341960

interaction effect between ke_raft, ke_nonraft: -0.018206859405361014

(Interpretation: positive effect: The distance is greater (= worse parameter choice))
*/

import sessl._
import sessl.mlrules._
import sessl.analysis.sensitivity._

// Reference values for calculating the sensitivity analysis results
// Reference = FractionSurfaceLRP6
object Reference {
  val ref: Trajectory[Double] = List(
    (0, 1),
    (10, 0.6408),
    (20, 0.5563),
    (30, 0.4554),
    (60, 0.3944)
  )
}

val factor = 4000 // nR_0 (factor for comparing with measurement data)


// Sensitivity analysis
analyze((params, objective) =>
  execute(

    new Experiment with Observation with ExpressionObservation with ParallelExecution with CSVOutput {

      model = "../models/M2_Microdomains.mlrj"

      simulator = HybridSimulator()
      //simulator = StandardSimulator()
      parallelThreads = 1

      val stoppingTime = Reference.ref.times.last + 1
      stopTime = stoppingTime
      replications = 1

      // Set initial species count as well as experiment specific reaction rate
      // coefficients

      set(
        "nR" <~ factor,
        "nL" <~ 2000
      )

      observeAt(Reference.ref.times: _*)
      //observeAt(0, 10, 20, 30, 60)
      val R = observe("R" ~ count("M/R"))
      val LR = observe("LR" ~ count("M/LR"))
      val RaftR = observe("RaftR" ~ count("M/Raft/R"))
      val RaftLR = observe("RaftLR" ~ count("M/Raft/LR"))

      val allR = observe(Expr(N => N(R) + N(LR) + N(RaftR) + N(RaftLR)))

      for ((name, value) <- params) {
        set(name <~ value)
      }

      withExperimentResult(results => {
        val distances = for (run <- results.runs) yield {
          val trajectory = run.trajectory(allR)
          val trajectory2 = trajectory.map {
            case (t, y) => (t, y / factor)
          }
          Misc.rmse(trajectory2, Reference.ref)
        }
        objective <~ distances.sum / distances.size
      })

      withRunResult(writeCSV)

    }
  )
) using new TwoLevelFullFactorialSetup {

  baseCase(
    "ke_raft" <~ 0.05,
    "ke_nonraft" <~ 0.05
  )
  sensitivityCase(
    "ke_raft" <~ 0.1,
    "ke_nonraft" <~ 0.1
  )

  withAnalysisResult(result => {
    result.printCompleteReport()
  })
}
