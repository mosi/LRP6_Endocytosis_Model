package org.jamesii.mlrules.r;

import org.jamesii.mlrules.experiment.Experiment;
import org.jamesii.mlrules.experiment.SimpleJob;
import org.jamesii.mlrules.experiment.SimulationRun;
import org.jamesii.mlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.mlrules.experiment.stop.StopConditionFactory;
import org.jamesii.mlrules.observation.AttributeListener;
import org.jamesii.mlrules.observation.Instrumenter;
import org.jamesii.mlrules.observation.IntervalObserver;
import org.jamesii.mlrules.observation.SpeciesCountListener;
import org.jamesii.mlrules.simulator.factory.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

/**
 * This is a thread-safe adapter to execute simulation runs in a blocking way.
 */
public class ExperimentAdapter {

    public ExperimentAdapter() {
        Logger.getGlobal().setLevel(Level.SEVERE);
    }

    private final ConcurrentMap<Integer, RunResults> results = new ConcurrentHashMap<>();
    private final ConcurrentMap<Integer, CountDownLatch> finished = new ConcurrentHashMap<>();

    public Experiment createExperiment(
            String modelLocation,
            String simulator,
            CountObservable[] countObservables,
            AttributeObservable[] attributeObservables,
            double observationInterval,
            double stopTime
    ) {

        SimulatorFactory simFactory;

        switch (simulator.toLowerCase()) {
            case "simple":
                simFactory = new SimpleSimulatorFactory(true, false);
                break;
            case "hybrid":
                simFactory = new HybridSimulatorFactory(
                        HybridSimulatorFactory.Integrator.DORMAND_PRINCE,
                        0.01,
                        0.03,
                        0.03,
                        true,
                        false);
                break;
            case "link":
                simFactory = new LinkSimulatorFactory(true, false);
                break;
            default:
                simFactory = new StandardSimulatorFactory(true, false);
        }

        Instrumenter instrumenter = (job, model) -> {
            IntervalObserver observer = new IntervalObserver(model, observationInterval);
            RunResults results = this.results.get(job.getID());
            for (CountObservable obs : countObservables) {
                results.countObservables.add(obs);
                observer.register(new SpeciesCountListener(obs.filter, pair -> {
                    results.addCountObservation(obs, pair.fst(), pair.snd());
                }));
            }
            for (AttributeObservable obs : attributeObservables) {
                results.attributeObservables.add(obs);
                observer.register(new AttributeListener(obs.filter, obs.index, pair -> {
                    results.addAttributeObservation(obs, pair.fst(), pair.snd());
                }));
            }
            return Collections.singleton(observer);
        };
        StopConditionFactory stopFactory = new SimTimeStopConditionFactory(stopTime);

        return new Experiment(
                modelLocation,
                simFactory,
                instrumenter,
                stopFactory,
                Runtime.getRuntime().availableProcessors() - 1);
    }

    public void runJob(Experiment experiment, Map<String, Object> parameters, int id) throws
            IOException, ModelNotSupportedException {

        results.put(id, new RunResults(id));
        finished.put(id, new CountDownLatch(1));

        experiment.addJob(new SimpleJob(id, parameters) {
            @Override
            protected void onSuccess(SimulationRun run) {
                finished.get(id).countDown();
            }
        });
    }

    public Object[][] getResults(int id) throws InterruptedException {
        finished.get(id).await();
        return translate(results.get(id));
    }

    public String[] columnNames(int id) {
        RunResults runResults = results.get(id);
        String[] names = new String[2 + runResults.countObservables.size() + runResults.attributeObservables.size()];
        names[0] = "run";
        names[1] = "time";
        for (int i = 0; i < runResults.countObservables.size(); i++) {
            names[2 + i] = runResults.countObservables.get(i).name;
        }
        for (int i = 0; i < runResults.attributeObservables.size(); i++) {
            names[2 + runResults.countObservables.size() + i] = runResults.attributeObservables.get(i).name;
        }
        return names;
    }

    public void finishExperiment(Experiment experiment) {
        experiment.finish();
    }

    public Map<String, Object> createParameterMap() {
        return new HashMap<>();
    }

    public void addParameter(Map<String, Object> parameterMap, String parameter, Object value) {
        parameterMap.put(parameter, value);
    }

    // repeat method with primitive types to let rJava discover it
    public void addParameter(Map<String, Object> parameterMap, String parameter, int value) {
        addParameter(parameterMap, parameter, Integer.valueOf(value));
    }

    public void addParameter(Map<String, Object> parameterMap, String parameter, double value) {
        addParameter(parameterMap, parameter, Double.valueOf(value));
    }

    private Object[][] translate(RunResults results) {
        Set<Double> timepoints = new HashSet<>();
        timepoints.addAll(results.countObservations.keySet());
        timepoints.addAll(results.attributeObservations.keySet());
        List<Double> timeline = new ArrayList<>(timepoints);
        timeline.sort(Double::compareTo);

        int numColumns = 2 + results.countObservables.size() + results.attributeObservables.size();

        ArrayList<Object[]> lines = new ArrayList<>();
        for (double t : timeline) {

            Map<AttributeObservable, Map<String, Object>> attributeObservations = results.attributeObservations.get(t);
            Map<CountObservable, Double> countObservations = results.countObservations.get(t);
            if (attributeObservations == null || attributeObservations.isEmpty()) {
                // one line for the entire observation time point
                Object[] line = new Object[numColumns];
                line[0] = results.runId;
                line[1] = t;
                if (countObservations != null) {
                    for (int j = 0; j < results.countObservables.size(); j++) {
                        line[2 + j] = countObservations.get(results.countObservables.get(j));
                    }
                }
                lines.add(line);
            } else {
                // one line per entity id
                // collect entity ids first
                Set<String> ids = new HashSet<>();
                for (Map<String, Object> obs : attributeObservations.values()) {
                    ids.addAll(obs.keySet());
                }
                for (String id : ids) {
                    Object[] line = new Object[numColumns];
                    line[0] = results.runId;
                    line[1] = t;
                    if (countObservations != null) {
                        for (int j = 0; j < results.countObservables.size(); j++) {
                            line[2 + j] = countObservations.get(results.countObservables.get(j));
                        }
                    }
                    int offset = 2 + results.countObservables.size();
                    for (int j = 0; j < results.attributeObservables.size(); j++) {
                        line[offset + j] = attributeObservations.get(results.attributeObservables.get(j)).get(id);
                    }
                    lines.add(line);
                }
            }
        }
        return lines.toArray(new Object[lines.size()][numColumns]);
    }

}
