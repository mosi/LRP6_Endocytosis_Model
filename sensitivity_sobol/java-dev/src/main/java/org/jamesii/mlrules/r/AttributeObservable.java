package org.jamesii.mlrules.r;

import java.util.Objects;

class AttributeObservable {
    final String name;
    final String filter;
    final int index;

    public AttributeObservable(String name, String filter, int index) {
        this.name = name;
        this.filter = filter;
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttributeObservable that = (AttributeObservable) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "AttributeObservable{" +
                "name='" + name + '\'' +
                ", filter='" + filter + '\'' +
                ", index=" + index +
                '}';
    }
}
