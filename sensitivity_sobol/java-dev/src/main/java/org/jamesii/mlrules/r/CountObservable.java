package org.jamesii.mlrules.r;

import java.util.Objects;

class CountObservable {
    final String name;
    final String filter;

    public CountObservable(String name, String filter) {
        this.name = name;
        this.filter = filter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountObservable that = (CountObservable) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "CountObservable{" +
                "name='" + name + '\'' +
                ", filter='" + filter + '\'' +
                '}';
    }
}
