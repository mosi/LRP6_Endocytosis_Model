package org.jamesii.mlrules.r;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class RunResults {
    final int runId;
    final List<CountObservable> countObservables;
    final List<AttributeObservable> attributeObservables;
    final Map<Double, Map<CountObservable, Double>> countObservations;
    final Map<Double, Map<AttributeObservable, Map<String, Object>>> attributeObservations;

    public RunResults(int runId) {
        this.runId = runId;
        this.countObservables = new ArrayList<>();
        this.attributeObservables = new ArrayList<>();
        this.countObservations = new HashMap<>();
        this.attributeObservations = new HashMap<>();
    }

    public void addCountObservable(CountObservable countObservable) {
        countObservables.add(countObservable);
    }

    public void addAttributeObservable(AttributeObservable attributeObservable) {
        attributeObservables.add(attributeObservable);
    }

    public void addCountObservation(CountObservable obs, double time, double value) {
        countObservations.putIfAbsent(time, new HashMap<>());
        countObservations.get(time).put(obs, value);
    }

    public void addAttributeObservation(AttributeObservable obs, double time, Map<String, Object> values) {
        attributeObservations.putIfAbsent(time, new HashMap<>());
        attributeObservations.get(time).put(obs, values);
    }
}
