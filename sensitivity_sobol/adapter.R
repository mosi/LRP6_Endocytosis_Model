mlrules.run.experiment <- function(
  modelfile,
  stopTime,
  observationStep,
  design,
  countObservables = list(),
  attributeObservables = list(),
  simulator = "standard"
) {
  # initialize the Java binding
  library(rJava)
  #TODO replace this with .jpackage()
  .jinit()
  for (lib in dir("./lib", pattern = "*.jar"))
    .jaddClassPath(paste("./lib", lib, sep = "/"))
  
  adapter <- .jnew("org/jamesii/mlrules/r/ExperimentAdapter")
  
  # convert observables
  countObservableObjects <- lapply(seq_len(length(countObservables)), function(i) {
    name <- names(countObservables)[[i]]
    if (name == "") {
      name <- countObservables[[i]]
    }
    filter <- countObservables[[i]]
    .jnew("org/jamesii/mlrules/r/CountObservable", name, filter)
  })
  attributeObservableObjects <- lapply(seq_len(length(attributeObservables)), function(i) {
    name <- names(attributeObservables)[[i]]
    if (name == "") {
      name <- attributeObservables[[i]][[1]]
    }
    filter <- attributeObservables[[i]][[1]]
    index <- attributeObservables[[i]][[2]]
    .jnew("org/jamesii/mlrules/r/AttributeObservable", name, filter, as.integer(index))
  })
  
  # create experiment object
  exp <- .jcall(
    adapter,
    "Lorg/jamesii/mlrules/experiment/Experiment;",
    "createExperiment",
    modelfile,
    simulator,
    .jarray(countObservableObjects, contents.class = "org/jamesii/mlrules/r/CountObservable"),
    .jarray(attributeObservableObjects, contents.class = "org/jamesii/mlrules/r/AttributeObservable"),
    observationStep,
    stopTime)
  
  #loop over design points and start runs (in parallel)
  results <- list()
  for (i in 1:nrow(design)) {
    paramMap <- .jcall(adapter, "Ljava/util/Map;", "createParameterMap")
    for (p in colnames(design)) {
      .jcall(adapter, "V", "addParameter", paramMap, p, design[[i, p]]);
    }
    .jcall(adapter, "V", "runJob", exp, paramMap, i)
    print(paste("job", i, "- submitted"))
  }
  #loop over design points and collect results
  for (i in 1:nrow(design)) {
    result <- .jcall(adapter, "[[Ljava/lang/Object;", "getResults", i, evalArray = FALSE)
    print(paste("job", i, "- collected results"))
    rresult <- lapply(result, function(x) { lapply(x, .jsimplify) })
    cols <- .jcall(adapter, "[Ljava/lang/String;", "columnNames", i)
    df <- data.frame(matrix(unlist(rresult), ncol = length(cols), byrow = T))
    colnames(df) <- cols
    for (p in colnames(design)) {
      # Add columns with parameter choice	
      df[[p]] <- design[i, p]
    }
    results[[i]] <- df
  }
  mergedResults <- do.call(rbind, results)
  return(mergedResults)
} 
