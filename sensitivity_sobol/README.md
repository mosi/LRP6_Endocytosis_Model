# How to calculate Sobol indices

* install [Apache Maven](https://maven.apache.org/)
* run either ```ExecuteMLRulesAndPlotResults.R ``` or ```ExecuteSesslandPlotResults.R```
* When starting ML-Rules directly from R (by executing ```ExecuteMLRulesAndPlotResults.R ```), you need to insatll [rJava](https://git.informatik.uni-rostock.de/mosi/mlrules-r-adapter).