import sessl._
import sessl.mlrules._


execute {
  new Experiment with Observation with ParallelExecution with CSVOutput with CSVInput {

    model = "../../models/M1_general.mlrj"

    simulator = HybridSimulator()
    //simulator = StandardSimulator()
    parallelThreads = -1

    val stoppingTime = 61
    stopTime = stoppingTime
    replications = 1

    designFromCSV("designOfExperiment.csv")

    observe("R"   ~ count("Membrane/Lrp6(uB)"))
    observe("LR"  ~ count("Membrane/Lrp6(B)"))

    observeAt(stoppingTime)
    withRunResult(writeCSV)
  }
}
