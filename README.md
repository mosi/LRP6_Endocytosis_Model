# LRP6 Endocytosis Model

This is a copy of all models and experiments used in the paper "Exploring mechanistic and temporal regulation of LRP6 endocytosis in canonical WNT Signaling" by F. Haack, K. Budde, and A. Uhrmacher (2020). The original repository is published [here](https://github.com/SFB-ELAINE/SI_LRP6_Endocytosis_Model). We have added a few experiments (see below).

## What is different to the GitHub repository


## Requirements for replicating all experiments from the paper

### Model
The models are located in ```/models/```.

### Replicating all experiments
1. Install R (and RStudio).
2. Run ```/experiments/ExecuteSesslandPlotResults.R```. All requirements, such as Sessl and ML-Rules, should automatically be downloaded. (This is currently working under Linux (Centos) and Windows 10.)
3. All plots are created within the new directory ```/experiments/Plots/```.