/*
Results:

parameter ke_raft:
  - individual effect:         -0.019935319640158405
  - total effect:              -0.0019825934640315367
  - total interaction effect:  0.01795272617612687

parameter ke_nonraft:
  - individual effect:         -0.10744763038340263
  - total effect:              -0.08949490420727577
  - total interaction effect:  0.017952726176126865

interaction effect between ke_raft, ke_nonraft: 0.01795272617612767

(Interpretation: positive effect: The distance is greater (= worse parameter choice))
*/

import sessl._
import sessl.mlrules._
import sessl.analysis.sensitivity._

// Reference values for calculating the sensitivity analysis results
// Reference = FractionSurfaceLRP6
object Reference {
  val ref: Trajectory[Double] = List(
    (0, 1),
    (10, 0.6408),
    (20, 0.5563),
    (30, 0.4554),
    (60, 0.3944)
  )
}

val factor = 4000 // nR_0 (factor for comparing with measurement data)


// Sensitivity analysis
analyze((params, objective) =>
  execute(

    new Experiment with Observation with ExpressionObservation with ParallelExecution with CSVOutput {

      model = "../models/M3_Wnt.mlrj"

      simulator = HybridSimulator()
      //simulator = StandardSimulator()
      parallelThreads = 1

      val stoppingTime = Reference.ref.times.last + 1
      stopTime = stoppingTime
      replications = 1

      // Set initial species count as well as experiment specific reaction rate
      // coefficients

      set(
        "nR" <~ factor,
        "nL" <~ 2000
      )

      observeAt(Reference.ref.times: _*)
      //observeAt(0, 10, 20, 30, 60)

      val Lrp6uPuB = observe("Lrp6uPuB" ~ count("Cell/Membrane/Lrp6(uP, uB)"))
      val Lrp6PuB = observe("Lrp6PuB" ~ count("Cell/Membrane/Lrp6(P, uB)"))
      val Lrp6uPB = observe("Lrp6uPB" ~ count("Cell/Membrane/Lrp6(uP, B)"))
      val Lrp6PB = observe("Lrp6PB" ~ count("Cell/Membrane/Lrp6(P, B)"))
      val Lrp6Axinu = observe("Lrp6Axinu" ~ count("Cell/Membrane/Lrp6Axin(u)"))
      val Lrp6Axinp = observe("Lrp6Axinp" ~ count("Cell/Membrane/Lrp6Axin(p)"))

      val Raft_Lrp6uPuB = observe("Raft_Lrp6uPuB" ~ count("Cell/Membrane/LR/Lrp6(uP, uB)"))
      val Raft_Lrp6PuB = observe("Raft_Lrp6PuB" ~ count("Cell/Membrane/LR/Lrp6(P, uB)"))
      val Raft_Lrp6uPB = observe("Raft_Lrp6uPB" ~ count("Cell/Membrane/LR/Lrp6(uP, B)"))
      val Raft_Lrp6PB = observe("Raft_Lrp6PB" ~ count("Cell/Membrane/LR/Lrp6(P, B)"))
      val Raft_Lrp6Axinu = observe("Raft_Lrp6Axinu" ~ count("Cell/Membrane/LR/Lrp6Axin(u)"))
      val Raft_Lrp6Axinp = observe("Raft_Lrp6Axinp" ~ count("Cell/Membrane/LR/Lrp6Axin(p)"))

      val allR = observe(Expr(N => N(Lrp6uPuB) + N(Lrp6PuB) + N(Lrp6uPB) + N(Lrp6PB) + N(Lrp6Axinu) + N(Lrp6Axinp) +
        N(Raft_Lrp6uPuB) + N(Raft_Lrp6PuB) + N(Raft_Lrp6uPB) + N(Raft_Lrp6PB) + N(Raft_Lrp6Axinu) + N(Raft_Lrp6Axinp)))

      for ((name, value) <- params) {
        set(name <~ value)
      }

      withExperimentResult(results => {
        val distances = for (run <- results.runs) yield {
          val trajectory = run.trajectory(allR)
          val trajectory2 = trajectory.map {
            case (t, y) => (t, y / factor)
          }
          Misc.rmse(trajectory2, Reference.ref)
        }
        objective <~ distances.sum / distances.size
      })

      withRunResult(writeCSV)

    }
  )
) using new TwoLevelFullFactorialSetup {

  baseCase(
    "ke_raft" <~ 0.05,
    "ke_nonraft" <~ 0.05
  )
  sensitivityCase(
    "ke_raft" <~ 0.1,
    "ke_nonraft" <~ 0.1
  )

  withAnalysisResult(result => {
    result.printCompleteReport()
  })
}
