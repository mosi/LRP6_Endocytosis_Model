# Requirements for Replicating all Experiments

## Model
The models are located in ../models/.

## Replicating all experiments
1. Install R (and RStudio).
2. Run "ExecuteSesslandPlotResults.R". All requirements, such as Sessl and ML-Rules, should automatically be downloaded. (This is currently working under Linux (Centos) and Windows 10.)
3. All plots are created within the new directory "/experiments/Plots"/.